use slint::{re_exports::{KeyEvent, KeyboardModifiers, PointerEvent,
                         PointerEventButton, PointerEventKind},
            SharedString};

fn format_modifiers(
    KeyboardModifiers { alt, control, meta, shift }: KeyboardModifiers,
) -> Option<String>
{
    let mut buf = String::new();

    if alt {
        buf.push('A');
    }
    if meta {
        if !buf.is_empty() {
            buf.push('-')
        }
        buf.push('M');
    }
    if control {
        if !buf.is_empty() {
            buf.push('-')
        }
        buf.push('C');
    }
    if shift {
        if !buf.is_empty() {
            buf.push('-')
        }
        buf.push('S');
    }

    if buf.len() == 0 {
        None
    } else {
        Some(buf)
    }
}

/*

Missing in input:

    - SysReq

        KeyPress event, serial 35, synthetic NO, window 0xa400001,
            root 0x421, subw 0x0, time 179419360, (1464,262), root:(3066,288),
            state 0x0, keycode 107 (keysym 0xff61, Print), same_screen YES,
            XLookupString gives 0 bytes:
            XmbLookupString gives 0 bytes:
            XFilterEvent returns: False

    - enter

        KeyPress event, serial 35, synthetic NO, window 0xa400001,
            root 0x421, subw 0x0, time 179549273, (1355,139), root:(2957,165),
            state 0x0, keycode 104 (keysym 0xff8d, KP_Enter), same_screen YES,
        "   XLookupString gives 1 bytes: (0d) "
        "   XmbLookupString gives 1 bytes: (0d) "
            XFilterEvent returns: False

    - multi (“right win”) ⇒ cause it’s the compose key?

        KeyPress event, serial 35, synthetic NO, window 0xa400001,
            root 0x421, subw 0x0, time 179678906, (1102,257), root:(2704,283),
            state 0x0, keycode 135 (keysym 0xff20, Multi_key), same_screen YES,
            XLookupString gives 0 bytes:
            XmbLookupString gives 0 bytes:
            XFilterEvent returns: True

    - arrow up

        KeyPress event, serial 35, synthetic NO, window 0xa400001,
            root 0x421, subw 0x0, time 179550593, (1355,139), root:(2957,165),
            state 0x0, keycode 111 (keysym 0xff52, Up), same_screen YES,
            XLookupString gives 0 bytes:
            XmbLookupString gives 0 bytes:
            XFilterEvent returns: False

    - arrow down

        KeyPress event, serial 35, synthetic NO, window 0xa400001,
            root 0x421, subw 0x0, time 179551137, (1355,139), root:(2957,165),
            state 0x0, keycode 116 (keysym 0xff54, Down), same_screen YES,
            XLookupString gives 0 bytes:
            XmbLookupString gives 0 bytes:
            XFilterEvent returns: False

    - scroll lock

        KeyPress event, serial 35, synthetic NO, window 0xa400001,
            root 0x421, subw 0x0, time 179419824, (1464,262), root:(3066,288),
            state 0x0, keycode 78 (keysym 0xff14, Scroll_Lock), same_screen YES,
            XLookupString gives 0 bytes:
            XmbLookupString gives 0 bytes:
            XFilterEvent returns: False

    - pause

        KeyPress event, serial 35, synthetic NO, window 0xa400001,
            root 0x421, subw 0x0, time 179420312, (1464,262), root:(3066,288),
            state 0x0, keycode 127 (keysym 0xff13, Pause), same_screen YES,
            XLookupString gives 0 bytes:
            XmbLookupString gives 0 bytes:
            XFilterEvent returns: False

    - function keys

        KeyPress event, serial 35, synthetic NO, window 0xa400001,
            root 0x421, subw 0x0, time 179332768, (986,477), root:(2588,503),
            state 0x0, keycode 67 (keysym 0xffbe, F1), same_screen YES,
            XLookupString gives 0 bytes:
            XmbLookupString gives 0 bytes:
            XFilterEvent returns: False

    - insert

        KeyPress event, serial 35, synthetic NO, window 0xa400001,
            root 0x421, subw 0x0, time 179280800, (1461,536), root:(3063,562),
            state 0x0, keycode 118 (keysym 0xff63, Insert), same_screen YES,
            XLookupString gives 0 bytes:
            XmbLookupString gives 0 bytes:
            XFilterEvent returns: False

    - page up

        KeyPress event, serial 35, synthetic NO, window 0xa400001,
            root 0x421, subw 0x0, time 179210463, (1170,479), root:(2772,505),
            state 0x0, keycode 112 (keysym 0xff55, Prior), same_screen YES,
            XLookupString gives 0 bytes:
            XmbLookupString gives 0 bytes:
            XFilterEvent returns: False

    - page down

        KeyPress event, serial 35, synthetic NO, window 0xa400001,
            root 0x421, subw 0x0, time 179211015, (1170,479), root:(2772,505),
            state 0x0, keycode 117 (keysym 0xff56, Next), same_screen YES,
            XLookupString gives 0 bytes:
            XmbLookupString gives 0 bytes:
            XFilterEvent returns: False
 */

fn escape_key_text(key: &SharedString) -> (bool, SharedString)
{
    let (is_text, key) = match key.as_ref() {
        "\u{0000}" => (false, "NUL"),
        "\u{0001}" => (false, "SOH"),
        "\u{0002}" => (false, "STX"),
        "\u{0003}" => (false, "ETX"),
        "\u{0007}" => (false, "^G"),
        "\u{0008}" => (false, "^H"),
        "\t" => (false, "\\t"),
        "\n" => (false, "\\n"),
        "\r" => (false, "CR"),
        "\u{000e}" => (false, "←"),
        "\u{000f}" => (false, "→"),
        "\u{0010}" => (false, "↑"),
        "\u{0011}" => (false, "↓"),
        "\u{001b}" => (false, "ESC"),
        "\u{007F}" => (false, "DEL"),
        any => (true, any),
    };

    (is_text, key.into())
}

fn pp_bytes_hex(bs: &[u8]) -> String
{
    let mut res = String::new();
    for b in bs {
        if !res.is_empty() {
            res.push(' ');
        }
        res.push_str(&format!("{:X}", b));
    }

    res
}

fn handle_key(
    mut buf: SharedString,
    ev: KeyEvent,
    pressed: bool,
    clear: bool,
) -> DisplayText
{
    use colored::*;

    let mods = format_modifiers(ev.modifiers);
    let (text_p, name) = escape_key_text(&ev.text);

    let key = if let Some(c) = mods {
        format!("<{}-{}>", c, name.as_str())
    } else if text_p {
        name.into()
    } else {
        format!("<{}>", name.as_str())
    };

    eprintln!(
        "{:>7}: [{}] raw: {}",
        if pressed { "PRESS".cyan().bold() } else { "RELEASE".green().bold() },
        key.bold(),
        pp_bytes_hex(ev.text.as_bytes())
    );

    if !pressed {
        /* Don’t update again on release. */
        return DisplayText { buf, clear, last: key.into() };
    }

    if clear {
        buf = "".into();
    }

    let buf = {
        match ev.text.as_ref() {
            "\n" => "".into(),
            "\u{0007}" | "\u{0008}" | "\u{007F}" if buf.is_empty() => buf,
            "\u{0007}" | "\u{0008}" | "\u{007F}" =>
                buf.chars()
                    .take(buf.chars().count() - 1)
                    .collect::<String>()
                    .into(),
            _ => {
                buf.push_str(key.as_str());
                buf
            },
        }
    };

    let (clear, buf) = if buf.is_empty() {
        (true, "<start typing>".into())
    } else {
        (false, buf)
    };

    DisplayText { buf, clear, last: key.into() }
}

/*
XXX: mouse wheel?
*/
fn handle_mouse(
    mut buf: SharedString,
    PointerEvent { button, kind }: PointerEvent,
    x: i32,
    y: i32,
    x0: i32,
    y0: i32,
) -> DisplayText
{
    use colored::*;

    let (kindsym, kindtext) = match kind {
        PointerEventKind::cancel => ("×", "CANCEL".red().bold()),
        PointerEventKind::up => ("⊥", "UP".magenta().bold()),
        PointerEventKind::down => ("⊤", "DOWN".purple().bold()),
    };

    let (buttonsym, buttontext) = match button {
        PointerEventButton::left => ("←", "LMB"),
        PointerEventButton::middle => ("↑", "MMB"),
        PointerEventButton::right => ("→", "RMB"),
        PointerEventButton::none =>
        /* All other buttons seem to map to this. */
            ("ø", "?MB"),
    };

    let last = if x == x0 && y == y0 {
        eprintln!("{:>7}: click {}  ({}, {})", kindtext, buttontext, x, y);
        format!("<{} {},{}>", kindsym, x, y).into()
    } else {
        eprintln!(
            "{:>7}: drag  {}  ({}, {}) ← orig: ({}, {})",
            kindtext, buttontext, x, y, x0, y0
        );
        format!("<{} {},{}→{},{}>", kindsym, x0, y0, x, y).into()
    };

    buf.push_str(&format!("<🐭{}>", buttonsym));
    DisplayText { buf, clear: false, last }
}

fn main()
{
    let win = W::new();

    win.on_terminate(move || {
        std::process::exit(0);
    });
    win.on_key_event(handle_key);
    win.on_mouse_event(handle_mouse);

    win.run();
}

slint::slint! {
    struct DisplayText := {
        buf: string,
        clear: bool,
        last: string,
    }
    W := Window {
        default-font-family: "CMU Sans Serif";
        default-font-size:   15pt;

        callback terminate;
        callback key-event(string, KeyEvent, bool, bool) -> DisplayText;
        callback mouse-event(string, PointerEvent, int, int, int, int) -> DisplayText;

        horizontal-stretch: 1;
        vertical-stretch: 1;
        forward-focus: scope;

        main-area := Rectangle {
            background: scope.has_focus ? #002b36 : #fdf6e3;
            callback set-display-text(DisplayText);
            property<bool> clear : true;

            set-display-text(text) => {
                buf.text = text.buf;
                clear = text.clear;
                last.text = text.last;
            }

            VerticalLayout {
                Text {
                    width: 100%;
                    vertical-alignment: center;
                    horizontal-alignment: center;
                    horizontal-stretch: 1;
                    vertical-stretch: 1;

                    font-weight: 400;
                    font-size: 30pt;

                    color: scope.has_focus ? #586e75 : #002b36;
                    text: scope.has_focus ? "focused" : "not focused";
                }

                Rectangle {
                    width: 100%;
                    vertical-stretch: 1;
                    color: scope.has_focus ? #073642 : #eee8d5;

                    HorizontalLayout {
                        last := Text {
                            font-weight: 800;
                            font-size: main-area.width / 15;
                            horizontal-alignment: center;
                            vertical-alignment: center;

                            color: scope.has_focus ? #fdf6e3 : #002b36;
                            text: "";
                        }
                    }
                }

                buf := Text {
                    width: 100%;
                    horizontal-alignment: center;
                    horizontal-stretch: 1;
                    vertical-stretch: 1;
                    vertical-alignment: center;

                    font-weight: 400;
                    font-size: 30pt;

                    color: scope.has_focus ? #fdf6e3 : #002b36;
                    text: "<start typing>";
                }
            }
        }

        touch := TouchArea {
            pointer-event(ev) => {
                main-area.set-display-text
                    (mouse-event
                        (buf.text,
                         ev,
                         mouse-x / 1px,
                         mouse-y / 1px,
                         pressed-x / 1px,
                         pressed-y / 1px));
            }
        }

        scope := FocusScope {
            property<string> key : "<none>";

            key-pressed(ev) => {
                if (ev.text == "q" && ev.modifiers.control) {
                    terminate();
                }

                main-area.set-display-text(key-event(buf.text, ev, true, main-area.clear));

                accept
            }
            key-released(ev) => {
                main-area.set-display-text(key-event(buf.text, ev, false, main-area.clear));
                accept
            }
        }
    }
}
