Synopsis
-----------------------------------------------------------------

An event viewer for SixtyFPS_, sort of what ``xev`` is to Xorg.

.. _SixtyFPS: https://sixtyfps.io/

Will catch mouse and keyboard events from ``TouchArea`` and
``FocusScope``, respectively.

Special bindings
-----------------------------------------------------------------

- ``<CR>``: clear the current input buffer.
- ``<C-q>``: quit.

Example
-----------------------------------------------------------------

.. image:: misc/sixev.png
   :width: 800px
   :align: center

